For future you guys can reference
Steps:

####

Register runner:
gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token TOKEN \
  --executor docker \
  --description "My Docker Runner" \
  --docker-image "docker" \
  --docker-privileged \
  --docker-volumes "/certs"

To Verify:
sudo gitlab-runner verify   (Without this command the VM will not talk to Instance Runner)

To Start: 
sudo gitlab-runner start

Extra command for Linux (Windows don't need):
gitlab-runner -debug run     (If without this command the Jobs will not run)
